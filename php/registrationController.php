<?php

require_once "utils.php";
require_once "userModel.php";
require_once "userService.php";


$user = new User($_POST);
$errorValidation = $user->isValid();

if (count($errorValidation) != 0) 
    return responce(400, $errorValidation);

if (checkUserByEmailInDataBase($user)) 
    return responce(400, "Пользователь с таким email уже зарегистрирован");

return responce(200, "Успешная регистрация");
?>
