<?php

/**
 * Модель пользователя
 */
class User
{
    public $id;
    public $name;
    public $surname;
    public $email;
    public $password;
    public $repeatPassword;

    /**
     * Конструктор
     */
    function __construct($array)
    {
        $this->id = $array["id"];
        $this->name = $array["name"];
        $this->surname = $array["surname"];
        $this->email = $array["email"];
        $this->password = $array["password"];
        $this->repeatPassword = $array["repeatPassword"];
    }

    /**
     * Сравнение пользователей по емайл
     */
    public function equalsForEmail(User $user)
    {
        if ($this->email != $user->email) return false;
        return true;
    }

    /**
     * Валидация данных пользователя
     */
    public function isValid()
    {
        $errorValidation = array();

        if (!preg_match("/.*@.*/", $this->email))
            $errorValidation[] = "Введите корректный EMail";
        if ($this->password != $this->repeatPassword)
            $errorValidation[] = "Пароли не совпадают";

        return $errorValidation;
    }
}

?>