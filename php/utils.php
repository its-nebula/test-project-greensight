<?php

/**
 * Формирование ответа
 */
function responce($code, $response)
{
    http_response_code($code);
    echo json_encode(
        array($code == 200 ? "successText" : "errorText" => $response),
        JSON_UNESCAPED_UNICODE
    );
}

?>