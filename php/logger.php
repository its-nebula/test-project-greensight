<?php
/**
 * Logger
 */
class Logger
{
    /**
     * Логирование ошибок
     */
    public function logError($text)
    {
        $file = fopen('../logs/log_file.log', 'a');
        fwrite($file, date( "Y-m-d H:i:s" ).' ERROR  '.$text."\n");
        fclose($file);
    }

    /**
     * Логирование справочной информации
     */
    public function logInfo($text)
    {
        $file = fopen('../logs/log_file.log', 'a');
        fwrite($file, date( "Y-m-d H:i:s" ).' INFO  '.$text."\n");
        fclose($file);
    }
}

?>