<?php

require_once "userModel.php";

/**
 * Список пользователей
 */
function getUsers()
{
    return array(
        new User(
            array(
                "id" => "1",
                "name" => "Иван",
                "surname" => "Иванов",
                "email" => "ivanov@mail.ru",
                "password" => "12345",
                "repeatPassword" => "12345"
            )
        ),
        new User(
            array(
                "id" => "2",
                "name" => "Татьяна",
                "surname" => "Иванов",
                "email" => "ivanova@mail.ru",
                "password" => "123456",
                "repeatPassword" => "123456"
            )
        ),
        new User(
            array(
                "id" => "3",
                "name" => "Олег",
                "surname" => "Вырин",
                "email" => "ov1998@mail.ru",
                "password" => "admin",
                "repeatPassword" => "admin"
            )
        )
    );
}

?>
