<?php 

require_once "userModel.php";
require_once "userDataBase.php";
require_once "logger.php";

/**
 * Проверка наличия пользователя в бд с таким же email
 */
function checkUserByEmailInDataBase(User $user)
{
    $arrayUsers = getUsers();
    $logger = new Logger();

    foreach($arrayUsers as $userDb)
        if ($user->equalsForEmail($userDb)){
            $logger -> logError("Пользователь с email ".$user ->email." уже зарегистрирован");
            return true;
        }

    $logger -> logInfo("Регистрация пользователя с email ".$user ->email);
    return false;
}

?>