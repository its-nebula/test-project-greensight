$(document).ready(function() {
    $('#registration').on("click", function() {

        var model = {};
        model.name = $("#name").val();
        model.surname = $("#surname").val();
        model.email = $("#email").val();
        model.password = $("#password").val();
        model.repeatPassword = $("#repeatPassword").val();

        $.ajax({
            url: 'php/registrationController.php',
            type: 'POST',
            cache: false,
            data: model,
            beforeSend: function() {
                $("#error").text("");
                $('#registration').attr("disabled", true);
            },
            success: function(data) {
                let response = JSON.parse(data);
                $("form").remove();
                $("#headerForm").text(response.successText)
                    .closest("div")
                    .append('<a href="index.html" class="btn btn-lg btn-outline-success mt-5">Вернуться к форме</a>');
            },
            error: function(error) {
                let response = JSON.parse(error.responseText);

                if (Array.isArray(response.errorText))
                    response.errorText.forEach(element => {
                        $("#error").append(element)
                            .append("<br>");
                    })
                else {
                    $("#error").append(response.errorText)
                        .append("<br>");
                }

                $('#registration').attr("disabled", false);
            }
        })
    })

});